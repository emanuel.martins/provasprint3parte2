package br.com.proway.provaSprint3parte2.interfaces;

import br.com.proway.provaSprint3parte2.models.Celular;

public interface IUsuario {
    void inserirCelular(Celular celular);
    void excluirCelular(int celularID);
}
