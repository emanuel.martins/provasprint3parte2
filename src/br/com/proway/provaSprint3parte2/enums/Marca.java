package br.com.proway.provaSprint3parte2.enums;

public enum Marca {
    APPLE(1, "Apple"),
    SAMSUNG(2, "Samsung"),
    LG(3, "LG"),
    XIAOMI(4, "Xiaomi");

    private final int id;
    private final String nome;

    Marca(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
}
