package br.com.proway.provaSprint3parte2;

import br.com.proway.provaSprint3parte2.enums.Marca;
import br.com.proway.provaSprint3parte2.models.Aparelho;
import br.com.proway.provaSprint3parte2.models.Celular;
import br.com.proway.provaSprint3parte2.models.Usuario;

public class Main {
    public static void main(String[] args) {
        Usuario usuario = new Usuario(1, "user@email.com", "User01");
        Aparelho aparelho = new Aparelho(1, "Celular", 500);
        Celular celular = new Celular(aparelho.getAparelhoId(), aparelho.getTipo(), aparelho.getQuantidadeUnitaria(),
                1, "S10", "128 GB", Marca.SAMSUNG);

        System.out.println("Aparelho: " + aparelho.getTipo());
        System.out.println("Celular: " + celular.getMarca() + " " + celular.getModelo());

        usuario.inserirCelular(celular);
        System.out.println(usuario.getNomeUsuario() + " tem: " + usuario.getCelularesPessoais().size() + " celulares");

        for (int i = 0; i < usuario.getCelularesPessoais().size(); i++) {
            System.out.println(" - " + usuario.getCelularesPessoais().get(i).getMarca()
                    + " " + usuario.getCelularesPessoais().get(i).getModelo() + "\n");
        }

        usuario.excluirCelular(celular.getCelularId());
        System.out.println(usuario.getNomeUsuario() + " tem: " + usuario.getCelularesPessoais().size() + " celulares");
    }
}
