package br.com.proway.provaSprint3parte2.models;

import br.com.proway.provaSprint3parte2.interfaces.IUsuario;

import java.util.ArrayList;

public class Usuario implements IUsuario {
    private int usuarioId;
    private String email;
    private String nomeUsuario;
    private ArrayList<Celular> celularesPessoais = new ArrayList<>();

    public Usuario(int usuarioId, String email, String nomeUsuario) {
        this.usuarioId = usuarioId;
        this.email = email;
        this.nomeUsuario = nomeUsuario;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public ArrayList<Celular> getCelularesPessoais() {
        return celularesPessoais;
    }

    public void setCelularesPessoais(ArrayList<Celular> celularesPessoais) {
        this.celularesPessoais = celularesPessoais;
    }

    @Override
    public void inserirCelular(Celular celular) {
        celularesPessoais.add(celular);
    }

    @Override
    public void excluirCelular(int celularID) {
        Celular celular = celularesPessoais.stream()
                .filter(x -> x.getCelularId() == celularID).findFirst().get();
        celularesPessoais.removeIf(celularParam -> celularParam.getCelularId() == celularID);

        System.out.println("Celular: " + celular.getMarca() + " retirado da lista pessoal");
    }
}
