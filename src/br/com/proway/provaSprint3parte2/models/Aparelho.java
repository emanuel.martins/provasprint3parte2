package br.com.proway.provaSprint3parte2.models;

public class Aparelho {
    private int aparelhoId;
    private String tipo;
    private int quantidadeUnitaria;

    public Aparelho(int aparelhoId, String tipo, int quantidadeUnitaria) {
        this.aparelhoId = aparelhoId;
        this.tipo = tipo;
        this.quantidadeUnitaria = quantidadeUnitaria;
    }

    public int getAparelhoId() {
        return aparelhoId;
    }

    public void setAparelhoId(int aparelhoId) {
        this.aparelhoId = aparelhoId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getQuantidadeUnitaria() {
        return quantidadeUnitaria;
    }

    public void setQuantidadeUnitaria(int quantidadeUnitaria) {
        this.quantidadeUnitaria = quantidadeUnitaria;
    }
}
