package br.com.proway.provaSprint3parte2.models;

import br.com.proway.provaSprint3parte2.enums.Marca;

public class Celular extends Aparelho {
    private int celularId;
    private String modelo;
    private String memoria;
    private Marca marca;

    public Celular(int aparelhoId, String tipo, int quantidadeUnitaria, int celularId, String modelo, String memoria, Marca marca) {
        super(aparelhoId, tipo, quantidadeUnitaria);
        this.celularId = celularId;
        this.modelo = modelo;
        this.memoria = memoria;
        this.marca = marca;
    }

    public int getCelularId() {
        return celularId;
    }

    public void setCelularId(int celularId) {
        this.celularId = celularId;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMemoria() {
        return memoria;
    }

    public void setMemoria(String memoria) {
        this.memoria = memoria;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }
}
